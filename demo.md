% Markdown with DOT
% Adam Monsen
% August 13th, 2014

<!--
take some underline/code/pre formatting inspiration from github (gollum) and
stackoverflow
-->
<style type="text/css">
  a:visited {
    color: blue;
  }
  body {
    font-family: Helvetica, arial, clean, sans-serif;
  }
  ol {
    line-height: 130%;
  }
  code,pre {
    background-color: #eaf7ff;
  }
  kbd {
    padding: .1em .6em;
    border: 1px solid #ccc;
    font-size: 11px;
    font-family: Arial,Helvetica,sans-serif;
    background-color: #f7f7f7;
    color: #333;
    -moz-box-shadow: 0 1px 0 rgba(0,0,0,0.2),0 0 0 2px #fff inset;
    -webkit-box-shadow: 0 1px 0 rgba(0,0,0,0.2),0 0 0 2px #fff inset;
    box-shadow: 0 1px 0 rgba(0,0,0,0.2),0 0 0 2px #fff inset;
    border-radius: 3px;
    display: inline-block;
    margin: 0 .1em;
    text-shadow: 0 1px 0 #fff;
    line-height: 1.4;
    white-space: nowrap;
  }
  .caption {
    font-style: italic;
  }
  abbr {
    border-bottom: dotted 1px;
  }
  table {
    border-style: none;
    border-collapse: collapse;
  }
  table th, table td {
    border-width: 1px;
    padding: 4px;
    border-style: inset;
    border-color: gray;
  }
</style>

# Graph

~~~ {.graphviz}
digraph G {
    user
    post
    comment

    user -> post [ arrowtail=odot, arrowhead=crow, dir=both ]
    user -> comment [ arrowtail=crow, arrowhead=crow, dir=both ]
    post -> comment [ arrowtail=crow, arrowhead=crow, dir=both ]
}
~~~

# Table

user|post
----|----
joe |hi  
ed  |cool  

# Code sample

~~~ {.python .numberLines}
# should be syntax-highlighted Python

def getHello():
    return "Hello World"

print(getHello())
~~~
